import glob
import os
import setuptools

with open("README.txt", "r") as fh:
    long_description = fh.read()

setuptools.setup(name = 'scaespy',
	             version = open("scaespy/_version.py").readlines()[-1].split()[-1].strip("\"'"),
	             description = 'scAEspy: a tool for autoencoder-based analysis of single-cell RNA sequencing data',
	             author = 'Andrea Tangherloni',
	             author_email = 'andrea.tangherloni@unibg.it',
	             url = 'https://gitlab.com/cvejic-group/scaespy',
	             keywords = ['autoencoder', 'machine learning', 'dimensionality reduction', 'single-cell RNA-data'],
	             license='LICENSE',
	             long_description=long_description,
	             classifiers = ['Programming Language :: Python :: 3.5', 'Programming Language :: Python :: 3.7'],
	             python_requires='>=3.5,<3.8',
	             packages=setuptools.find_packages(where='scaespy'),
	             package_dir={'': 'scaespy'},
	             py_modules=[os.path.splitext(os.path.basename(path))[0] for path in glob.glob('scaespy/*.py')],
	             install_requires=['NumPy>=1.18',
	                               'Pandas>=0.25',
	                               'matplotlib>=3.1',
	                               'scikit-learn>=0.21',
	                               'seaborn>=0.9',
	                               'Tensorflow>=1.12,<=1.15'],
	             entry_points = {'console_scripts': ['scaespy = cli.scaespy:main']})